<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cadastro Agenda</title>
    <?php
      include('../css/bootstrap.php');
    ?>
    
</head>
<body>

<?php
  include('menu.php');
?>

<!--Aqui colocaremos codigos em HTML -->

<div class="panel panel-default">
		<div class="panel-heading">
				<h4>Formulário de Cadastramento de Contatos</h4>
		</div>
		<div class="panel-body">
			<form action="">
					<div class="for-group col-md-12 col-xs-12">
							<label for="nome">Nome Completo</label><span class="text-red">*</span>
							<input type="text" name="nome" id="nome" riquired class="form-control">
					</div>
					<div class="for-group col-md-6 col-xs-6">
							<label for="foneres">Telefone Residencial</label>
							<input type="text" name="fone_res" id="fone_res" riquired class="form-control">
					</div>
					<div class="for-group col-md-6 col-xs-6">
							<label for="fonecel">Telefone Celular</label><span class="text-red">*</span>
							<input type="text" name="fone_cel" id="fone_cel" riquired class="form-control">
					</div>
					<div class="for-group col-md-12 col-xs-12">
							<label for="email">Email</label><span class="text-red">*</span>
							<input type="email" name="email" id="email" riquired class="form-control">
					</div>
					<div class="for-group col-md-4 col-xs-4">
							<label for="datnasc">Data Nascimento</label><span class="text-red">*</span>
							<input type="date" name="nome" id="nome" riquired class="form-control">
					</div>
					<div class="for-group col-md-12 col-xs-12">
							<label for="enderco">Endereço</label><span class="text-red">*</span>
							<input type="text" name="endereco" id="endereco" riquired class="form-control">
					</div>
					<div class="for-group col-md-12 col-xs-12">
							<label for="bairro">Bairro</label><span class="text-red">*</span>
							<input type="text" name="bairro" id="bairro" riquired class="form-control">
					</div>
					<div class="for-group col-md-12 col-xs-12">
							<label for="cep">CEP</label><span class="text-red">*</span>
							<input type="text" name="cep" id="cep" riquired class="form-control">
					</div>
					<div class="for-group col-md-12 col-xs-12">
							<label for="uf">UF</label><span class="text-red">*</span>
							<input type="text" name="uf" id="uf" riquired class="form-control">
					</div>
					<div class="for-groupcol-md-12 col-xs-12">
							<label for="foto">Foto</label><span class="text-red">*</span>
							<input type="text" name="foto" id="foto" riquired class="form-control">
					</div>
					<div class="for-group col-md-4 col-xs-12">
							<label for="facebook">Facebook</label><span class="text-red">*</span>
							<input type="text" name="facebook" id="facebook" riquired class="form-control">
					</div>
					<div class="for-group col-md-4 col-xs-12">
							<label for="twitter">Twitter</label><span class="text-red">*</span>
							<input type="text" name="twitter" id="twitter" riquired class="form-control">
					</div>
					<div class="for-group col-md-4 col-xs-12">
							<label for="instagram">Instagram</label><span class="text-red">*</span>
							<input type="text" name="instagram" id="instagram" riquired class="form-control">
					</div>
			</form>
		</div>
		<div class="panel-footer text-center">
				<button class="btn btn-default">
						<i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Voltar
				</button>
				<button type="submit" class="btn btn-success">
						<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Gravar
				</button>
		</div>
</div>

<?php
  include('../js/jquery.php');
?>
</body>
</html>